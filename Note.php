<?php
require_once(dirname(__FILE__)."/UE.php");

class Note {
    /* Titre de la note : ex [cc-total] Projet LDVH */
    /* Note : ex 12/20 */
    /* UE : ex obj(4I403) */
    private $titre, $note, $ue, $periode;
    /* Vrai si la note n'a pas déjà été vue (ajoutée au fichier). Vrai par
       défaut, sera modifiée plus tard si nécessaire */
    private $nouvelle;

    public function __construct($titre, $note, $ue, $periode) {
        $this->titre = $titre;
        $this->note = $note;
        $this->ue = $ue;
        $this->periode = $periode;
        $this->nouvelle = true;
    }

    /* Getters */

    public function getTitre() {
        return $this->titre;
    }

    public function getNote() {
        return $this->note;
    }

    public function getUE() {
        return $this->ue;
    }

    public function getPeriode() {
        return $this->periode;
    }

    public function getNouvelle() {
        return $this->nouvelle;
    }

    /* Pas de setters pour le titre, la note et l'UE, ces informations ne
     * changeront pas */

    /* Appellé si cette note était déjà là la fois précédente */
    public function dejaVu() {
        $this->nouvelle = false;
    }

}
?>