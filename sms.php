<?php
const URL_SMS_API = "https://smsapi.free-mobile.fr/sendmsg";

function sendSMS($user, $pass, $msg) {

    $params = array(
        "user" => $user,
        "pass" => $pass,
        "msg" => $msg
    );

    $url = URL_SMS_API . '?' . http_build_query($params);
    $ch = curl_init($url);
    /* curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); */
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    $document = curl_exec($ch);
    if($document === false) {
        echo "Erreur CURL : \n";
        echo curl_error($ch);
    }

    curl_close($ch);
}
?>