DBUFRProbe
==========

Lancer une requête sur DBUFR pour vérifier s'il y a des nouvelles notes
Si c'est le cas :  
Envoyer un SMS avec le nom de l'UE et la note  
Envoyer un message sur Slack avec le nom de l'UE si une URL est fournie  


Utilisation
-----------
Obtenir une copie du dépot  
Appeler le script configure (./configure)  
Editer le fichier créé : localData.txt (des détails sont donnés dedans)  
Pour effectuer une requête : php lanceur.php  

Le script scriptLanceur.sh peut être utilisé avec crontab (des détails sont dans
le fichier) pour un appel périodique  

----

TODO list
---------

- [x] Faire la classe UE avec seulement les variables et les accesseurs
- [x] Faire la classe Note avec seulement les variables et les accesseurs
- [x] Faire la classe PageDBUFR avec seulement les variables, les accesseurs
   et des fonctions vide
- [x] Ajouter à PageDBUFR la méthode parse
- [x] PageDBUFR : modifier la visibilité de la méthode getNouvellesNotes
- [x] Créer le mécanisme du fichier de configuration
- [x] Créer le lanceur pour pouvoir tester les accès à l'arbre
- [x] Ajouter les inclusion de fichiers (dépendances)
- [x] Ajouter des commentaires si besoin
- [x] Tester le programme jusque là
- [x] Vérifier le retour des fonctions
- [x] Masquer les erreurs de loadHTML()
- [x] Ajouter à PageDBUFR les méthodes ajouterUEs/ajouterNotes
- [x] Ajouter à PageDBUFR la méthode rechercherNotes
- [x] Ajouter à PageDBUFR la méthode getNouvellesNotes
- [x] Tester le retour des nouvelles notes
- [x] Résoudre le bug des Notes qui manquent (index unique ?)
- [x] Annuler l'envoi de SMS si c'est le premier appel (ajout de toutes les notes
- [x] Ajouter l'envoi de SMS
- [x] Ajouter l'appel régulier (periodes de pointe et periodes creuses)
- [x] Ajouter l'envoi d'un message sur Slack
- [x] Modifier les commentaires de LocalData
- [x] Rendre l'envoi sur Slack optionnel
- [x] Créer le fichier de configuration par un script pour ne pas avoir à
   ajouter d'informations confidentielles  à git
- [x] Donner l'état d'avancement du projet et les instructions pour l'utiliser
- [x] Commenter l'utilisation de scriptLanceur avec cron
- [ ] lanceur.php pour les nouvelles notes : remplacer if($document) par 
   if($premierAppel) ??
- [ ] Tester l'appel récurrent sur un serveur sans SSH
- [ ] Envoi d'un message sur Slack, ajouter un fichier avec les responsable des
  UEs pour le nom affiché
- [ ] Gérer les SMS trop longs (>160 chars)
- [ ] Améliorer la gestion des logs


Tests d'appel récurrent sur un serveur sans SSH
-----------------------------------------------

Faire un script qui appelle le programme puis attends 10mn (par exemple) et
recommence indéfiniement

Faire un script php qui change les droits d'exécution du script précédent et le
lance en background

Ou faire un script qui appelle Cron

mettre ces deux scripts sur le serveur et appeler le second