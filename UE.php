<?php
require_once(dirname(__FILE__)."/Note.php");

class UE {
    /* Code de l'UE : ex LI101 */
    /* Titre de l'UE : ex Programmation récursive */
    /* Periode de l'UE : ex 2013fev */
    private $code, $titre, $periode;
    /* Notes qui seront ajoutées après avoir parsé le document */
    private $notes;

    public function __construct($code, $titre, $periode) {
        $this->code = $code;
        $this->titre = $titre;
        $this->periode = $periode;
        $this->notes = array();
    }

    /* Getters */
    public function getCode() {
        return $this->code;
    }

    public function getTitre() {
        return $this->titre;
    }

    public function getPeriode() {
        return $this->periode;
    }

    /* Pas de setters pour le code, le titre et la periode, ces informations ne
     * changeront pas */
    
    public function addNote($note) {
        $this->notes[] = $note;
    }
}
?>