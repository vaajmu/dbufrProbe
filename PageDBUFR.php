<?php
require_once(dirname(__FILE__)."/UE.php");
require_once(dirname(__FILE__)."/Note.php");
require_once(dirname(__FILE__)."/simple_html_dom.php");

class PageDBUFR {
    /* Tableau de toutes les ues et de toutes les notes */
    private $ues, $notes;
    /* Contenu de la page DBUFR */
    /* L'arbre vaut faux si le chargement du document a échoué */
    private $document, $arbre;
    /* Tableau des nouvelles notes */
    private $nouvellesNotes;

    /* document est une chaine de caractère : c'est le contenu de la pahe HTML
     * de DBUFR */
    public function __construct($document) {
        $this->document = $document;
        $this->arbre = false;
        $this->ues = array();
        $this->notes = array();
        $this->nouvellesNotes = array();
    }

    /* Parse le document vers un arbre et récupère les UEs et les notes */
    public function parseDocument() {
        /* Parser le document, rechercher la table des UEs et la table des
           notes, instancier les UEs et les notes */
        $this->arbre = str_get_html($this->document);
        $tables = $this->arbre->find('table.Table');
        $this->ajouterUEs($tables[0]);
        $this->ajouterNotes($tables[1]);
        $this->rechercherNotesPrecedentes();
    }

    /* Parcours la balise table des UEs et les enregistre */
    private function ajouterUEs($table) {
        /* Pour toutes les lignes de la table, rechercher les TDs, passer à la
           ligne suivante si c'est la ligne d'en-tête, déterminer les champs et
           créer/ajouter la nouvelle UE */
        foreach($table->find('tr') as $ligne) {
            $cells = $ligne->find('td');
            if(count($cells) == 0) continue;

            $as = $cells[1]->find('a'); /* variable tmp pour éviter l'erreur de
                                         * compilation */
            $code = trim($as[0]->plaintext);
            $titre = trim($cells[3]->plaintext);
            $periode = trim($cells[2]->plaintext);
            $indiceDansTab = $code.$periode;

            $this->ues[$indiceDansTab] = new UE($code, $titre, $periode);
        }
    }

    /* Parcours la balise table des notes et les enregistre */
    private function ajouterNotes($table) {
        /* Pour toutes les lignes de la table, rechercher les TDs, passer à la
           ligne suivante si c'est la ligne d'en-tête, sinon déterminer les
           champs et créer/ajouter la nouvelle note */
        foreach($table->find('tr') as $ligne) {
            $cells = $ligne->find('td');
            if(count($cells) == 0) continue;

            $titre = trim($cells[1]->plaintext);
            $note = trim($cells[2]->plaintext);
            if(!preg_match("@^([^-]*)-+(.*)$@", $cells[0]->plaintext, $resRE)) {
                echo "erreur UE et periode\n";
                exit(1);
            }
            $ue = $resRE[1];
            $periode = $resRE[2];
            $indiceDansTableau = $ue.$periode.$titre.$note;
            $indiceUEDansTab = $ue.$periode;

            /* On stocke dans un tableau associatif pour pouvoir les retrouver
               rapidement ensuite */
            $this->notes[$indiceDansTableau] = 
                new Note($titre, $note, $this->ues[$indiceUEDansTab], $periode);
        }
    }

    /* Ouvre le fichier de notes en (lectire/écriture) et pour chaque note du
     * fichier, la recherche dans les instances de notes et les marques comme
     * "déjà vu" */
    private function rechercherNotesPrecedentes() {
        /* Récupérer les notes dans un tableau de lignes, parser la ligne,
           retrouver la note en fonction de l'UE et de la periode et la noter
           comme déjàVu. Pour toutes les notes qui sont nouvelles, les ajouter
           au fichier */

        /* Si le fichier n'existe pas, on suppose que c'est le premier appel */
        if(file_exists(dirname(__FILE__).'/dejaVues.csv')) {
            $tabNotesDejaVues = file(dirname(__FILE__).'/dejaVues.csv');
            if($tabNotesDejaVues === false) {
                echo "erreur lecture fichier\n";
                exit(1);
            }

            foreach($tabNotesDejaVues as $noteDejaVue) {
                if(!preg_match("@^([^;]+)-([^;]+);([^;]+)+;(.+)$@", $noteDejaVue, $resRE)) {
                    echo "Erreur syntaxe fichier dejaVues.csv\n";
                    exit(1);
                }
                $indiceTabNote = $resRE[1].$resRE[2].$resRE[4].$resRE[3];
                $this->notes[$indiceTabNote]->dejaVu();
            }
        }

        $fic = fopen(dirname(__FILE__).'/dejaVues.csv', 'a');
        if($fic === false) {
            echo "erreur ouverture fichier\n";
            exit(1);
        }

        foreach($this->notes as $note) {
            if($note->getNouvelle()) {
                $this->nouvellesNotes[] = $note;
                $chaine = $note->getUE()->getCode() . "-" . $note->getPeriode() . ";" . 
                    $note->getNote() . ";" . $note->getTitre() . "\n";
                if(fwrite($fic, $chaine) === false) {
                    echo "erreur ouverture fichier\n";
                    exit(1);
                }
            }
        }
        fclose($fic);
    }

    /* Retourne un tableau de toutes les nouvelles notes. Le tableau est rempli
       au moment du parse, dans la fonction rechercherNotesPrecedentes */
    public function getNouvellesNotes() {
        return $this->nouvellesNotes;
    }

    /* Pas de getters ni de setters pour ues, notes, document et arbre. Ces variables sont
     * gérées en interne */

}
?>