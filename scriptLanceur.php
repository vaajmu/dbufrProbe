<?php

/* Script pour vérifier les nouvelles notes */

// Définit le fuseau horaire par défaut à utiliser. Disponible depuis PHP 5.1
date_default_timezone_set('Europe/Paris');

/* Rediriger la sortie vers les logs */
ob_start();
/* Ajouter la date pour le log */
echo 'Date : ' . date(DATE_RFC2822) . "\n";

/* Procédure principale */
include(dirname(__FILE__)."/lanceur.php");

/* Récupérer la sortie et l'ajouter aux logs */
$contents = ob_get_contents();
/* ob_end_clean(); */
file_put_contents(dirname(__FILE__)."/logs.txt", $contents, FILE_APPEND);

?>