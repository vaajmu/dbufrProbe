<?php
class LocalData {
    /* Numéro et mot de passe de l'étudiant */
    /* Identifiant et mot de passe de l'opérateur */
    /* Url du compte Slack */
    private $numEtu, $mdpEtu, $idTel, $passTel, $urlSlack;
    /* Singleton */
    private static $instance = null;

    private function __construct() {
        /* Charge le fichier propre à la mahine */
        $lignes = file(dirname(__FILE__)."/localData.txt");
        if($lignes === false) {
          echo "erreur file\n";
          exit(1);
        }

        /* Extraction du numéro d'étudiant */
        if(!preg_match("@^numEtu (\d{7})$@", $lignes[0], $resRE)) {
            echo "erreur parse fichier localData.txt";
            exit(1);
        }
        $this->numEtu = $resRE[1];

        /* Extraction du mot de passe */
        if(!preg_match("@^mdpEtu (.*)$@", $lignes[1], $resRE)) {
            echo "erreur parse fichier localData.txt";
            exit(1);
        }
        $this->mdpEtu = $resRE[1];

        /* Extraction de l'identifiant pour l'opérateur */
        if(!preg_match("@^idTel (.*)$@", $lignes[2], $resRE)) {
            echo "erreur parse fichier localData.txt";
            exit(1);
        }
        $this->idTel = $resRE[1];

        /* Extraction du mot de passe pour l'opérateur */
        if(!preg_match("@^passTel (.*)$@", $lignes[3], $resRE)) {
            echo "erreur parse fichier localData.txt";
            exit(1);
        }
        $this->passTel = $resRE[1];

        /* Extraction de l'URL Slack */
        if(!preg_match("@^urlSlack (.*)$@", $lignes[4], $resRE)) {
            echo "erreur parse fichier localData.txt";
            exit(1);
        }
        $this->urlSlack = $resRE[1];
    }

    /* Créer/retourner le singleton */
    public static function getLocalData() {
        if(self::$instance == null)
            self::$instance = new LocalData();
        return self::$instance;
    }

    /* Getters */

    public function getNumEtu() {
        return $this->numEtu;
    }

    public function getMdpEtu() {
        return $this->mdpEtu;
    }

    public function getIdTel() {
        return $this->idTel;
    }

    public function getPassTel() {
        return $this->passTel;
    }

    public function getUrlSlack() {
        return $this->urlSlack;
    }

    /* Pas de setters, les variables sont affectés par le fichier localData
     * seulement */
}
?>