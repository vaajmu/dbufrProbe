#! /bin/bash
# scriptLanceur.sh
# Est appelé par crontab
# Appelle le script lanceur.php

# Dans la suite, l'exécutable se trouve dans ~/workspace/dbufrProbe/, il faudra
# le modifier si ce n'est pas le cas

# Utilisation de Cron :

# Lister la table cron :
#   crontab -l

# Éditer la table cron :
#   crontab -e

# Une entrée pour cron ;
#  1ère colonne, les minutes : de 0 à 59
#  2ème colonne, les heures : de 0 à 23
#  3ème colonne, le jour du mois : de 0 à 31
#  4ème colonne, les mois : de 0 à 12
#  5ème colonne, le jour de la semaine : de 0 à 7 (dimanche correspondant à 0 ou 7)
#  6ème colonne, la tâche à exécuter

# Exemple pour un appel toutes les 30 minutes
#   Editer la table cron : `crontab -e`, puis ajouter la ligne :
#   0,30 * * * * ~/workspace/dbufrProbe/scriptLanceur.sh
#   en remplaçant le chemin d'accès si nécessaire, puis fermer l'éditeur de texte.

# Aller dans le dossier du script (changer le chemin si nécessaire)
cd ~/workspace/dbufrProbe

# Ecrire la date dans le fichier de log
date >> logs.txt
# Appeler le script
php lanceur.php 1>&2 >> logs.txt

exit 0
