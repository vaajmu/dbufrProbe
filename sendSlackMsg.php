<?php
require_once(dirname(__FILE__)."/LocalData.php");
require_once(dirname(__FILE__)."/PageDBUFR.php");
require_once(dirname(__FILE__)."/Note.php");
require_once(dirname(__FILE__)."/UE.php");

function sendSlackMsg($note, $url) {
    $nomUE = $note->getUE()->getTitre();
    $username = ($nomUE == "") ? "J. Sopena" : "DBUFR";
    $message = "Les notes de " . $nomUE . " sont en ligne !!";

    $objJSON = json_encode(array(
        "channel"       =>  "#general",
        "username"       =>  $username,
        "text"          =>  $message
    ));
    $data = "payload=" . $objJSON;

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);

    echo $result . "\n";
}

?>