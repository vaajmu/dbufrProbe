<?php
/* Inclusion des fichiers */
require_once(dirname(__FILE__)."/sms.php");
require_once(dirname(__FILE__)."/LocalData.php");
require_once(dirname(__FILE__)."/PageDBUFR.php");
require_once(dirname(__FILE__)."/Note.php");
require_once(dirname(__FILE__)."/sendSlackMsg.php");

/* Définition de constantes */
const URL_DBUFR = "https://www-dbufr.ufr-info-p6.jussieu.fr/lmd/2004/master/auths/seeStudentMarks.php";

$premierAppel = !file_exists(dirname(__FILE__).'/dejaVues.csv');

/* Chargement du fichier de configuration (local à la machine) */
$configLocale = LocalData::getLocalData();

/* Faire la requête sur DBUFR */
/* Décommenter le paragraphe suivant pour effectuer la requête */

$auth = $configLocale->getNumEtu().":".$configLocale->getMdpEtu();
$ch = curl_init(URL_DBUFR);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
curl_setopt($ch, CURLOPT_USERPWD, $auth);
$document = curl_exec($ch);
curl_close($ch);
/* $document = file_get_contents("dbufr.html"); */

if($document === false) {
    echo "Erreur curl_exec\n";
    exit(1);
}

/* Instancier PageDBUFR et demander à parser le document */
$pageDBUFR = new PageDBUFR($document);
$pageDBUFR->parseDocument();

/* Demander les nouvelles notes */
$nouvellesNotes = $pageDBUFR->getNouvellesNotes();

/* Pour chaque nouvelle notes, envoyer un SMS */
foreach($nouvellesNotes as $note) {
    echo "Note de " . $note->getUE()->getCode() . "\n";

    /* Si ce n'est pas le premier appel), envoyer une requête */
    if(!$premierAppel) {
        $msg = "Une nouvelle note est disponible :\n" . 
            $note->getUE()->getCode() . " - " . $note->getUE()->getTitre() . 
            "\n" . $note->getTitre() . "\n" . $note->getNote();
        sendSMS($configLocale->getIdTel(), $configLocale->getPassTel(), $msg);

        /* Envoi d'une requête sur Slack si l'URL a été fournie */
        $urlSlack = $configLocale->getUrlSlack();
        if($urlSlack != "") sendSlackMsg($note, $urlSlack);
    }
}

?>